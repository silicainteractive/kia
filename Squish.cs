using Flixna.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KIA {
    public class Squish : IEntity
    {
        private Vector2 position;
        private Vector2 velocity;
        private Texture2D texture;
        private int currentFrame = 0;
        private float fps = 8;
        public Squish(Texture2D texture){ 
            this.texture = texture;
            position = Vector2.One * 200;
            velocity = Vector2.UnitX * 20;
        }
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            var sourceRect = new Rectangle(63*currentFrame,0, 63, 72);
            spriteBatch.Begin();
            spriteBatch.Draw(texture,position,sourceRect, Color.HotPink, 0,Vector2.Zero,Vector2.One,SpriteEffects.None, 1);
            spriteBatch.End();
        }

        float timer = 0f;
        public void Update(GameTime gameTime)
        {
            timer += (float) gameTime.ElapsedGameTime.TotalMilliseconds;
            if(timer > (1000/fps)) {
                currentFrame ++;
                if(currentFrame == 2){
                    position += velocity;
                }
                if(currentFrame > 3)
                    currentFrame = 0;
                timer = 0f;
            }

            if(position.X > 500) {
                position = new Vector2(0,position.Y);
            }
        }
    }
}