using Flixna.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace KIA {
    public class BaseState: State {
        
        private readonly SpriteBatch spriteBatch;
        public BaseState(GraphicsDevice graphicsDevice) {
            spriteBatch = new SpriteBatch(graphicsDevice);

        }
        public override void Draw(GameTime gameTime) => children.ForEach(c => c.Draw(spriteBatch, gameTime));

        public override void Update(GameTime gameTime) => children.ForEach(c => c.Update(gameTime));
        public override void LoadContent(ContentManager content)
        {
            var rect = content.Load<Texture2D>("Signs");
            AddChild(new Sign(rect));
            var squish = content.Load<Texture2D>("char");
            AddChild(new Squish(squish));
       }
    }
}