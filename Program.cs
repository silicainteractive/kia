﻿using System;

namespace KIA
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new KiaGame())
                game.Run();
        }
    }
}
