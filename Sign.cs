using Flixna.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace KIA {
    public class Sign : IEntity
    {
        private Texture2D texture;
        private Rectangle front;
        private Rectangle back;
        private Rectangle current;
        private Vector2 position;
        private Rectangle sourceRect;
        public Sign(Texture2D texture) {
            this.texture = texture;

            back = new Rectangle(0,0,30,32);
            front = new Rectangle(30,0,30,32);
            current = back;

            position = new Vector2(200,200);
            sourceRect = new Rectangle(200,200,30*5, 32*5);
        }
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(texture, new Vector2(200,200), current, Color.White, 0,Vector2.Zero, Vector2.One * 5, SpriteEffects.None, 0);
            spriteBatch.End();
        }

        private bool foo = true;
        public void Update(GameTime gameTime)
        {
            var mousePos = Mouse.GetState().Position;
            if(Mouse.GetState().LeftButton == ButtonState.Released) {
                foo = true;
            }
            if(foo && Mouse.GetState().LeftButton == ButtonState.Pressed && sourceRect.Contains(mousePos)) {
                foo = false;
                current = (current == front)? back : front;
            }
        }
    }
}